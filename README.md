# timetrial-bot

群馬高専文芸部で,文章のトレーニングとして用いられている,TT(timetrial)のためのbotです。

## timetrialとは

timetrialとは、お題を1つ決めて、200字の原稿用紙1枚でお題に関する短い物語を書くトレーニングです。

## install

```sh
git clone https://gitlab.com/eniehack/timetrial-bot.git
cd timetrial-bot
poetry install
```

## usage

### start

optionの `-f` でお題を保存するファイル、 `-t` でDiscordから取得したtokenを指定します。

```sh
poetry run python ./timetrial_bot/main.py -f {text file} -t {discord bot token}
```

### usage

メンション先を`@timetrial-bot`としていますが、実際はDiscord側に登録した名前を入力することになります。

`@timetrial-bot random`で起動時に選択されたテキストファイルから1つお題を入手します。
`@timetrial-bot add {word}` で `-f` で指定したデキストファイルへ`{word}`を追加します。
