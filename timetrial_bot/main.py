import sys
import argparse
import discord
import random

def readfile(filename):
    words = list()
    with open(filename, "r", encoding="utf-8") as f:
        for l in f.readlines():
            words.append(l)
    return words

def add_word(filename, word):
    with open(filename, "a", encoding="utf-8") as f:
        f.write("{}\n".format(word))

filename = sys.argv[1]
parser = argparse.ArgumentParser("Timetrial Discord Bot")
parser.add_argument("-f", "--file", help="words file", required=True)
parser.add_argument("-t", "--token", help="discord bot token", required=True)
args = parser.parse_args()

client = discord.Client()

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if client.user in message.mentions:
        cmd = message.content.split()

        if cmd[1] == "random":
            words = readfile(args.file)
            index = random.randint(1, len(words)) - 1
            await message.channel.send("お題: {}".format(words[index]))
        if cmd[1] == "add":
            add_word(args.file, cmd[2])

client.run(args.token)
